# %%

from emobpy import Mobility, DataBase, Consumption, HeatInsulation, BEVspecs, ModelSpecs, Availability, Charging, Export
import pandas as pd
from emobpy.tools import set_seed
set_seed()

# %% [markdown]
# ## Context
# -------------------------------
# 
# We have a CSV file with electricity consumption time series and grid availability time series of 19 electric trucks. The data is in MW and MWh with hourly resolution and 8760 hours. The data is contained in a CSV file. There are two modes of trucks: BEV and OBEV (Overhead line BEV), and the battery size for BEV is 651 and for OBEV is 181 kWh.
# 
# ### Problem
# ------------------------------
# 
# We want to simulate a charging strategy 'balanced' to get the grid demand time series. As we already have the electricity consumption and the grid availability, we should not need to generate the three first time series with emobpy (mobility, consumption and availability). However, to use emobpy, we must create the time series in order from 1 to 4. 
# 
# ### Approach
# ------------------------------
# We will force emobpy to generate one dummy time series for each type (mobility, consumption and availability). Then, we will replace the dummy Availability time series with our existing data in CSV. There we will also modify some other parameters for consistency. 
# 
# **Note**: As we are working with trucks and our database of BEV only contains personal car models, we will add a new truck model to the database.

# %% [markdown]
# ------------------------------------------------
# ### Step 1: Vehicle mobility time series
# ------------------------------------------------
# a) generation of a time series
# 
# ------------------------------------------------

# %%
m = Mobility(config_folder='config_files')
m.set_params(
             name_prefix="dummy",
             total_hours=8760, # one year
             time_step_in_hrs=1, # 60 minutes
             reference_date="01/01/2030"
            )
m.set_stats(
            stat_ntrip_path="TripsPerDay.csv",
            stat_dest_path="DepartureDestinationTrip.csv",
            stat_km_duration_path="DistanceDurationTrip.csv",
            )
m.set_rules()  # no rules files as this will be a dummy profile
m.run()
m.save_profile(folder="db", description='8760 hrs 60 min step ref-date 01/01/2030')

# %% [markdown]
# ------------------------------------------------------
# ### Step 2: Driving consumption time series
# ------------------------------------------------------
# a) Vehicle model configuration
# 
# ------------------------------------------------------

# %%
DB = DataBase('db')
DB.loadfiles()

# %%
mname = list(DB.db.keys())[0]        # getting the id of the first mobility profile
HI = HeatInsulation(True)            # Creating the heat insulation by copying the default configuration
BEVS = BEVspecs()                    # Database that contains BEV models
Truck = ModelSpecs(('Truck_brand','Truck_model',2030), BEVS) # creating a new model
Parameters = {
'battery_charging_eff':0.96,
'battery_discharging_eff':0.96,
'transmission_eff':0.9,
'auxiliary_power':0,
'cabin_volume':1,
'hvac_cop_heating':1,
'hvac_cop_cooling':1,
'power':400,
'curb_weight':12000,
'axle_ratio':4,
'height':3,
'width':3.2,
'drag_coeff':0.69,
'battery_cap':1000      # this will be changed further below
}
Truck.add(Parameters)
Truck.add_fallback_data()
Truck.add_calculated_param()
Truck.addtodb()
BEVS.save()             # these two lines save the new model in our json file 

# %% [markdown]
# ----------------------------------------------------------------------
# b) Calculate consumption for each trip and generate the time series
# 
# ----------------------------------------------------------------------

# %%
c = Consumption(mname, Truck)
c.load_setting_mobility(DB)
c.run(
    heat_insulation=HI,
    weather_country='DE',
    weather_year=2016,
    passenger_mass=75,                   # kg
    passenger_sensible_heat=70,          # W
    passenger_nr=1.5,                    # Passengers per vehicle including driver
    air_cabin_heat_transfer_coef=20,     # W/(m2K). Interior walls
    air_flow = 0.02,                     # m3/s. Ventilation
    driving_cycle_type='WLTC',           # Two options "WLTC" or "EPA"
    road_type=0,                         # For rolling resistance, Zero represents a new road.
    road_slope=0
    )
c.save_profile('db')

# %% [markdown]
# -------------------------------------------------------------------
# ### Step 3: Grid availability time series
# -------------------------------------------------------------------
# a) probability distribution and power rating for charging stations
# 
# -------------------------------------------------------------------

# %%
DB.update() 
cname = c.name   
station_distribution = {              
    'prob_charging_point': {
        'errands': {'public': 1, 'none': 0},
        'home': {'public': 1, 'none': 0},
        'driving': {'none': 0.90,'fast':0.10}}, 
    'capacity_charging_point': {
        'public': 200,
        'none': 0,
        'fast': 300}
}

# %% [markdown]
# --------------------------------
# b) Generate time series
# 
# --------------------------------

# %%
ga = Availability(cname, DB)
ga.set_scenario(station_distribution)
ga.run()
ga.save_profile('db')

# %% [markdown]
# ----------------------------------------------------------
# ### Step 4: Grid electricity demand time series
# ----------------------------------------------------------

# %%
DB.update()
aname = ga.name                            # getting the id of the availability profile
custom = pd.read_csv('trucks_profiles.csv', header=[0, 1, 2]) \
                                .stack([0, 1, 2]) \
                                .reset_index([1, 2, 3]) \
                                .rename(columns={'level_1':'ev','level_2':'type','level_3':'mode',0:'value'})

# %%
custom_battery_cap = {'BEV':651.11,'OBEV':181.41}
soc_init = 0.85

for (ev,mode), table in custom.groupby(['ev','mode']):
    df = table.set_index(['type','mode','ev'],append=True).unstack([1])['value']*1000 # Data was in MW and MWh > kW(h)
    ged = Charging(aname)
    ged.load_scenario(DB)
    ged.set_sub_scenario("balanced")  # here the charging strategy
    ged.change_battery_cap = custom_battery_cap[mode]
    ged.soc_init = soc_init
    ged.points = ['available','none']
    ged.states = ['parked','driving']
    ged.capacity_charging_point = {}
    tb = ged.profile.drop(['soc','consumption kWh','count'],axis=1)
    tb.loc[:,'distance'] = 0 # distance as zero. We can only provide energy consumption time series (distance is not relevant for this example)
    tb.loc[:,'consumption'] = df['consumption'].values     # here we add the consumption time series from CSV
    tb.loc[:,'charging_cap'] = df['charging_cap'].values   # here we add the charging_cap time series from CSV
    tb.loc[tb['consumption'] > 0.0,'state'] = 'driving'
    tb.loc[tb['consumption'] == 0.0,'state'] = 'parked'
    tb.loc[tb['charging_cap'] > 0.0,'charging_point'] = 'available'
    tb.loc[tb['charging_cap'] == 0.0,'charging_point'] = 'none'
    ged.profile = tb
    ged.run()
    ged.name = '_'.join([mode,ev,ged.name])   # change name of the files
    ged.save_profile(mode)


# %% [markdown]
# ------------------------------------------------------------
# ### Export all time-series in 'db' folder to [DIETER](https://diw-evu.gitlab.io/dieter_public/dieterpy/) format
# -----------------------------------------------------------------------------------------------------------
# After exporting see the two CSV files at "db" folder
# 
# --------------------------------------------------------------------

# %%
def get_group(DB):
    collector = []

    for name in DB.db.keys():
        df = DB.db[name]['timeseries']
        df['ev'] = name.split('_')[1]
        collector.append(df)

    df = pd.concat(collector)
    dt = df.set_index('ev', append=True)[['charge_grid']].unstack([-1])['charge_grid']
    tb = dt.rename(columns={k: 'ev' + str(int(''.join(list(filter(str.isdigit, k))))) for k in dt.columns})/1000
    return tb

# %%
mode = 'InflexBEV'
DB = DataBase(mode)
DB.loadfiles()
df = get_group(DB)
df.to_csv(mode + '.csv', index=False)

# %%
mode = 'InflexOBEV'
DB = DataBase(mode)
DB.loadfiles()
df = get_group(DB)
df.to_csv(mode + '.csv', index=False)
