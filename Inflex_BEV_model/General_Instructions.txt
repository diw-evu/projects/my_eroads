In this folder, you will find the script to generate exogenous truck electricity demand (The profiles follow a charging strategy instead of a power sector endogenous decision).
    - First, we use the tool emobpy (https://pypi.org/project/emobpy/0.6.2/) to generate InflexBEV.csv and InflexOBEV.csv [See 01_get_inflex.html]
    - Second, we enable DIETER to include the exogenous time series by exporting the CSV files data to iteration_data.xlsx.

Note:
Including inflexible load truck profiles in the power sector optimization model DIETER, the two files, InflexBEV.csv and InflexOBEV.csv, are transcripted to '../project_files/iterationfiles/iteration_data.xlsx' with the following tags scen_BEV_exo and scen_OBEV_exo respectively.
