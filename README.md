# My eRoads Project


This project used GAMS 44.4

Compatibility

`conda create -n <give a name here> python=3.10`

`pip install dieterpy==1.6.2`

`pip install symbolx==0.4.2`

`pip install gams --find-links <here path to python api in gams system directory>`

`pip install plotly jupyter`

The repository contains files and data for analyzing the impact of direct and indirect electrification of heavy-duty vehicles on power sectors. The repository enables scenario evaluation and generates concise reports and visual representations of the research results. It serves as a valuable resource for understanding the effects of electrification on heavy-duty vehicles.


- `/HDV_Mission_Profiles`: The folder provides a workbook that contains the building process of the HDV mission profiles with relevant assumptions and parameters. These profiles are essential inputs for modeling the effect of the electrification of HDV in the power sector.

- `/Inflex_BEV_model`: This folder contains the code used to generate inflexible electricity demand using the Python tool emobpy.

- `/project_files/data_input`: This folder stores the input data required for the research. It includes information on investment costs of generating technologies, storage, electrolyzer, and hydrogen storage. Additionally, it contains data on fuel costs, time series of renewable resources availability, country power demand, hydrogen demand, and e-fuel demand.

- `/project_files/iteration`: This folder is used to store scenarios parameters and time series data related to electric HDV. It includes information on electricity demand for driving, charging availability, and inflexible demand from the grid. The folder is crucial for conducting iterations and analyzing various scenarios.

- `/project_files/model`: This folder contains the power sector model DIETER, which is written in the GAMS language. DIETER integrates the power sector with hydrogen production for HDV and battery-electric vehicles. The model plays a central role in examining the impact of direct and indirect electrification of heavy-duty vehicles.

- `/project_files/data_output`: This folder stores the resulting DIETER GDX files from 40 scenarios, which are compressed in zip files. These files hold valuable information for the research. The zip file also contains additional data utilized by the symbolx Python tool to convert scenario variables into a Python object called Symbol. The symbols generated are utilized in the `02_get_report.ipynb` file to process the results and generate charts that are included in the research paper.

Additionally, the project folder includes the following files:
- `01_run_optimizations.py`: This Python script likely plays a role in running optimizations or specific calculations for the project.
- `02_get_report.ipynb`: This Jupyter Notebook file is used to generate a report based on the research results. It utilizes the symbols generated from the DIETER GDX files to process the data and generate charts.
- `02_get_report.html`: This HTML file is an output of the `02_get_report.ipynb` notebook, providing a web-based version of the report.


