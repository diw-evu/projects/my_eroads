



con5a_custom_total_generation(n)$(phi_min_res(n)>0)..
    sum( h , sum( map_n_tech(n,res), sum( dis$(sameas(res,dis)), G_L(n,dis,h))) + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RSVR_OUT(n,rsvr,h))
%reserves%$ontext
            - sum( reserves_do , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_do,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_do,rsvr,h))) * phi_reserves_call(n,reserves_do,h))
            + sum( reserves_up , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_up,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_up,rsvr,h))) * phi_reserves_call(n,reserves_up,h))
$ontext
$offtext

%prosumage%$ontext
            + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h)) + sum( map_n_res_pro(n,res) , G_MARKET_PRO2M(n,res,h) + G_RES_PRO(n,res,h))
$ontext
$offtext
)
    =E= phi_min_res(n) * sum( h ,
            sum( map_n_tech(n,dis) , G_L(n,dis,h)) + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RSVR_OUT(n,rsvr,h))

%reserves%$ontext
            - sum( reserves_do , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_do,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_do,rsvr,h))) * phi_reserves_call(n,reserves_do,h))
            + sum( reserves_up , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_up,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_up,rsvr,h))) * phi_reserves_call(n,reserves_up,h))
$ontext
$offtext

%prosumage%$ontext
            + sum( map_n_res_pro(n,res) , phi_res(n,res,h) * N_RES_PRO(n,res) - CU_PRO(n,res,h))
$ontext
$offtext
                                )
;







con5a_custom_total_demand(n)$(phi_min_res(n)>0)..

        sum( h , sum( map_n_tech(n,res), sum( dis$(sameas(res,dis)), G_L(n,dis,h))) + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RSVR_OUT(n,rsvr,h))
%reserves%$ontext
            - sum( reserves_do , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_do,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_do,rsvr,h))) * phi_reserves_call(n,reserves_do,h))
            + sum( reserves_up , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_up,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_up,rsvr,h))) * phi_reserves_call(n,reserves_up,h))
$ontext
$offtext

%prosumage%$ontext
            + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h)) + sum( map_n_res_pro(n,res) , G_MARKET_PRO2M(n,res,h) + G_RES_PRO(n,res,h))
$ontext
$offtext
)
        =G= phi_min_res(n) * sum( h ,
            + d(n,h)

%P2H2%$ontext
            + sum( h2_channel,  H2_TOTAL_ELECTRICITY_OUT(n,h2_channel,h))
$ontext
$offtext

%EV%$ontext
            + sum( map_n_ev(n,ev), EV_GED(n,ev,h)/eta_ev_in(n,ev) + EV_DIRECT(n,ev,h)$(sw_catenary(n,ev)=1))
$ontext
$offtext

%reserves%$ontext
            - sum( reserves_do , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_do,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_do,rsvr,h))) * phi_reserves_call(n,reserves_do,h))
            + sum( reserves_up , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_up,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_up,rsvr,h))) * phi_reserves_call(n,reserves_up,h))
$ontext
$offtext

%prosumage%$ontext
            + sum( map_n_res_pro(n,res) , phi_res(n,res,h) * N_RES_PRO(n,res) - CU_PRO(n,res,h))
$ontext
$offtext
)

            + sum( h , sum( map_n_sto(n,sto) , STO_IN(n,sto,h) - STO_OUT(n,sto,h)) )
  
%EV%$ontext  
            + sum( h , sum( map_n_ev(n,ev), EV_CHARGE(n,ev,h) - EV_DISCHARGE(n,ev,h) - EV_GED(n,ev,h)/eta_ev_in(n,ev)))
$ontext
$offtext
;






conX_h2_total_electricity_consumption(n,h2_channel,h)$feat_node('hydrogen',n)..

    H2_TOTAL_ELECTRICITY_OUT(n,h2_channel,h)
  
    =E=
   
    + 0.001 * (
*       prod
            sum( h2_tech , H2_E_H2_IN(n,h2_tech,h2_channel,h) )
*       aux_prod_site
            + sum( h2_tech , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * H2_PROD_AUX_IN(n,h2_tech,h2_channel,h) )
*       hydration_liquefaction
            + (h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) * H2_PROD_AUX_OUT(n,h2_channel,h) )
*       prod_site_storage
            + (h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h) * h2_sto_p_ed(n,h2_channel) )
*       aux_bftrans
            + (h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) * (sum(h2_tech,h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT(n,h2_channel,h)) )
*       aux_bflp_storage
            + (h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) * H2_AUX_BFLP_STO_IN(n,h2_channel,h) )
*       lp_storage
            + (h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h) * h2_lp_sto_ed(n,h2_channel) )
*       dehydration_evaporation
            + (( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_ed(n,h2_channel) * H2_LP_STO_OUT(n,h2_channel,h) )
*       aux_bfMP_storage
            + (h2_aux_bfMP_sto_sw(n,h2_channel) * h2_aux_bfMP_sto_ed(n,h2_channel) * H2_DEHYD_EVAP_OUT(n,h2_channel,h) )
*       MP_storage
            + (h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h) * h2_Mp_sto_ed(n,h2_channel) )
*       aux_bffilling_storage
            + (h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) * H2_AUX_BFHP_STO_IN(n,h2_channel,h) )
*       filling storage
            + (h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h) * h2_hp_sto_ed(n,h2_channel) )
*       aux_bffuel
            + (h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) * H2_HP_STO_OUT(n,h2_channel,h) )
*       aux_recon_site
            + sum( h2_tech_recon , h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_ed(n,h2_channel,h2_tech_recon) * H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
            )
;
