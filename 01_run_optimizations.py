import os
import time
import shutil
from dieterpy.scripts import runopt
from dieterpy.config import settings

settings.PROJECT_DIR_ABS = os.getcwd()
settings.update_changes()

runopt.main('project_variables1.csv')
time.sleep(3)
runopt.main('project_variables2.csv')
time.sleep(3)
runopt.main('project_variables3.csv')
time.sleep(3)
print("============== Done ====================")

if os.path.exists(settings.TMP_DIR_ABS):
    shutil.rmtree(settings.TMP_DIR_ABS)
    print("============== TMP deleted ====================")
